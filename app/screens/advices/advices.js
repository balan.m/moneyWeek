import React from 'react';
import {
  FlatList,
  Image,
  View,
  TouchableOpacity
} from 'react-native';
import {
  RkText,
  RkCard, RkStyleSheet
} from 'react-native-ui-kitten';
import ActionButton from 'react-native-action-button';
import {SocialBar} from '../../components';
import {data} from '../../data';
let moment = require('moment');


export class AdviceList extends React.Component {
  static navigationOptions = {
    title: 'ADVICE'.toUpperCase()
  };

  constructor(props) {
    super(props);
    let testCard = data.getCards();
    console.log(testCard);
    console.log(testCard[0])
    this.data = data.getAdvices();
    console.log(this.data);
    console.log(this.data[0]);
    this.renderItem = this._renderItem.bind(this);
    this.addAdvice = this._addAdvice.bind(this);
  }

  _keyExtractor(post, index) {
    return post.id;
  }
  _addAdvice(){
	  this.props.navigation.navigate("AddAdvice");
  }
  _renderItem(info) {
    return (
      <TouchableOpacity
        delayPressIn={70}
        activeOpacity={0.8}
        onPress={() => this.props.navigation.navigate('Article', {id: info.item.id})}>
        <RkCard style={styles.card}>
          <View rkCardHeader style={{flex:1, flexDirection:'row'}}>
            <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}}>
              <View style={{justifyContent: 'flex-start'}}>
                <RkText rkType='header4'>{info.item.name}</RkText>
                <RkText rkType='secondary2 hintColor'>{info.item.date}</RkText>
              </View>
              <View style={styles.buy}>
                  <RkText rkType='header4' style={{color: 'white',textAlign: 'center',textAlignVertical: 'center', paddingBottom: 3}}>BUY</RkText>
              </View>
            </View>
          </View>
           <View style={[styles.userInfo, styles.bordered]}>
              <View style={styles.section}>
                <RkText rkType='header4' style={styles.space}>764</RkText>
                <RkText rkType='secondary1 hintColor'>Buy</RkText>
              </View>
              <View style={styles.section}>
                <RkText rkType='header4' style={styles.space}>774</RkText>
                <RkText rkType='secondary1 hintColor'>Target</RkText>
              </View>
              <View style={styles.section}>
                <RkText rkType='header4' style={styles.space}>754</RkText>
                <RkText rkType='secondary1 hintColor'>Stop Loss</RkText>
              </View>
            </View>
          <View rkCardContent>
           
          </View>
        </RkCard>
      </TouchableOpacity>
    )
  }

  render() {
    return (
    <View Style={styles.root}>
      <FlatList
        data={this.data}
        renderItem={this.renderItem}
        keyExtractor={this._keyExtractor}
        style={styles.container} />
      	<ActionButton buttonColor="rgba(231,76,60,1)"
      		onPress={this.addAdvice}
      />
      </View>
    )
  }
}

let styles = RkStyleSheet.create(theme => ({
  container: {
    backgroundColor: theme.colors.screen.scroll,
    paddingHorizontal: 14,
    paddingVertical: 8
  },
  card: {
    marginVertical: 8
  },
  footer: {
    paddingTop: 16
  },
  time: {
    marginTop: 5
  },
  buy: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'green',
    width: 50,
    height: 30
  },
   baseText: {
	  fontFamily: 'Cochin'
  },
  titleText: {
	  fontSize: 20,
	  fontWeight: 'bold'
  },
  userInfo: {
    flexDirection: 'row',
    paddingVertical: 18,
  },
  bordered: {
    borderTopWidth: 1,
    borderColor: theme.colors.border.base
  },
  space: {
    marginBottom: 3
  },
  section: {
    flex: 1,
    alignItems: 'center'
  }
}));