import React from 'react';
import {
  ScrollView,
  View,
  StyleSheet,
  AppRegistry, Text,AlertIOS
} from 'react-native';
import {
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkTheme,
  RkStyleSheet,
} from 'react-native-ui-kitten';
import {data} from '../../data';
import {Avatar} from '../../components';
import {SocialSetting} from '../../components';
import {FontAwesome} from '../../assets/icons';
import {GradientButton} from '../../components';
import * as addAdviceService from '../../service/addAdviceService';
import AutoComplete from 'react-native-autocomplete';
import Countries from '../../data/raw/countries.json';
export class AddAdvice extends React.Component {
  static navigationOptions = {
    title: 'Advice'.toUpperCase()
  };

  constructor(props) {
    super(props);
    this.state = {
      tradeName: this.tradeName,
      tradeTye: this.tradeType,
      priceValue: this.priceValue,
      data: []
    }
    this.addAdviceDetail = this.addAdviceDetail.bind(this);
    this.onTyping = this.onTyping.bind(this);
    this.onSelect = this.onSelect.bind(this);
  }
  addAdviceDetail(){
	  //addAdviceService.addAdvice(this.state.tradeName,this.state.tradeType,this.state.priceValue).then(advices =>{
		//  console.log(advices);
		  this.props.navigation.navigate("AdviceList");
	//  });
  };
  onTyping(text) {
    const countries = Countries
        .filter(country => country.name.toLowerCase().startsWith(text.toLowerCase()))
        .map(country => country.name);
 
    this.setState({ data: countries });
  }
 
  onSelect(value) {
    AlertIOS.alert('You choosed', value);
  }
  render() {
    return (
      <ScrollView style={styles.root}>
      <View style={styles.container}>
              <AutoComplete
                style={styles.autocomplete}
       
                suggestions={this.state.data}
                onTyping={this.onTyping}
                onSelect={this.onSelect}
       
                placeholder="Search for a country"
                clearButtonMode="always"
                returnKeyType="go"
                textAlign="center"
                clearTextOnFocus
       
                autoCompleteTableTopOffset={10}
                autoCompleteTableLeftOffset={20}
                autoCompleteTableSizeOffset={-40}
                autoCompleteTableBorderColor="lightblue"
                autoCompleteTableBackgroundColor="azure"
                autoCompleteTableCornerRadius={8}
                autoCompleteTableBorderWidth={1}
       
                autoCompleteFontSize={15}
                autoCompleteRegularFontName="Helvetica Neue"
                autoCompleteBoldFontName="Helvetica Bold"
                autoCompleteTableCellTextColor={'dimgray'}
       
                autoCompleteRowHeight={40}
                autoCompleteFetchRequestDelay={100}
       
                maximumNumberOfAutoCompleteRows={6}
              />
            </View>

          <View style={styles.section}>
             

            <View style={styles.row}>
              <RkTextInput label='Trade Name'
                           value={this.state.tradeName}
                           rkType='right clear'
                           onChangeText={(text) => this.setState({tradeName: text})}/>
            </View>
            <View style={styles.row}>
              <RkTextInput label='Trade Type'
                           value={this.state.tradeType}
                           onChangeText={(text) => this.setState({tradeType: text})}
                           rkType='right clear'/>
            </View>
            <View style={styles.row}>
              <RkTextInput label='Price Value'
                           value={this.state.priceValue}
                           onChangeText={(text) => this.setState({priceValue: text})}
                           rkType='right clear'/>
            </View>
                   </View>

          
          <GradientButton rkType='large' style={styles.button} text='SAVE' onPress={this.addAdviceDetail}/>
      </ScrollView>
    )
  }
}

let styles = RkStyleSheet.create(theme => ({
  root: {
    backgroundColor: theme.colors.screen.base
  },
  header: {
    backgroundColor: theme.colors.screen.neutral,
    paddingVertical: 25
  },
  section: {
    marginVertical: 25
  },
  heading: {
    paddingBottom: 12.5
  },
  row: {
    flexDirection: 'row',
    paddingHorizontal: 17.5,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: theme.colors.border.base,
    alignItems: 'center'
  },
  button: {
    marginHorizontal: 16,
    marginBottom: 32
  },
  autocomplete: {
    alignSelf: 'stretch',
    height: 50,
    margin: 10,
    marginTop: 50,
    backgroundColor: '#FFF',
    borderColor: 'lightblue',
    borderWidth: 1,
  },
 container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
}));