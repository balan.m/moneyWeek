import  React, { Component }  from 'react';
import {
  AppRegistry, StyleSheet, Text, View, AlertIOS
} from 'react-native';
 
import AutoComplete from 'react-native-autocomplete';
import Countries from '../../data/raw/countries.json';
const styles = StyleSheet.create({
  autocomplete: {
    alignSelf: 'stretch',
    height: 50,
    margin: 10,
    marginTop: 50,
    backgroundColor: '#FFF',
    borderColor: 'lightblue',
    borderWidth: 1,
  },
 container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  }
});
 
export class AutocompleteExample extends Component {
 
  state = { data: [] }
 
  constructor(props) {
    super(props);
    this.onTyping = this.onTyping.bind(this)
  }
 
  onTyping(text) {
    const countries = Countries
        .filter(country => country.name.toLowerCase().startsWith(text.toLowerCase()))
        .map(country => country.name);
 
    this.setState({ data: countries });
  }
 
  onSelect(value) {
    AlertIOS.alert('You choosed', value);
  }
 
  render() {
    return (
      <View style={styles.container}>
        <AutoComplete
          style={styles.autocomplete}
 
          suggestions={this.state.data}
          onTyping={this.onTyping}
          onSelect={this.onSelect}
 
          placeholder="Search for a country"
          clearButtonMode="always"
          returnKeyType="go"
          textAlign="center"
          clearTextOnFocus
 
          autoCompleteTableTopOffset={10}
          autoCompleteTableLeftOffset={20}
          autoCompleteTableSizeOffset={-40}
          autoCompleteTableBorderColor="lightblue"
          autoCompleteTableBackgroundColor="azure"
          autoCompleteTableCornerRadius={8}
          autoCompleteTableBorderWidth={1}
 
          autoCompleteFontSize={15}
          autoCompleteRegularFontName="Helvetica Neue"
          autoCompleteBoldFontName="Helvetica Bold"
          autoCompleteTableCellTextColor={'dimgray'}
 
          autoCompleteRowHeight={40}
          autoCompleteFetchRequestDelay={100}
 
          maximumNumberOfAutoCompleteRows={6}
        />
      </View>
    );
  }
}