import {FontIcons} from '../../assets/icons';
import * as Screens from '../../screens/index';
import _ from 'lodash';

export const MoneyWeekRoutes = [
  {
    id: 'Login',
    title: 'Login',
    icon: FontIcons.login,
    screen: Screens.Login
  },
  {
    id: 'Signup',
    title: 'Signup',
    icon: FontIcons.signup,
    screen: Screens.SignUp
  },
  {
    id: 'Dashboard',
    title: 'Dashboard',
    action: 'DrawerOpen',
    icon: FontIcons.dashboard,
    screen: Screens.Dashboard
  },
  {
    id: 'Profile',
    title: 'Profile',
    icon: FontIcons.dashboard,
    screen: Screens.Profile
  },
  {
    id: 'AdviceList',
    title: 'Advice',
    icon: FontIcons.dashboard,
    screen: Screens.AdviceList
  },
  {
	    id: 'AddAdvice',
	    title: 'Advice',
	    icon: FontIcons.dashboard,
	    screen: Screens.AddAdvice
	  },
    {
      id: 'AutocompleteExample',
      title: 'AutocompleteExample',
      icon: FontIcons.dashboard,
      screen: Screens.AutocompleteExample
    }
]

export const MoneyWeekMenus = [
  {
    id:'Dashboard',
    title: 'Dashboard',
    icon: FontIcons.dashboard,
    screen: Screens.Dashboard,
    children: []
  },
  {
    id:'Profile',
    title: 'Profile',
    icon: FontIcons.dashboard,
    screen: Screens.Profile,
    children: []
  },
  {
    id:'About As',
    title: 'About as',
    icon: FontIcons.dashboard,
    screen: Screens.AboutAs,
    children: []
  },
  {
    id:'Settings',
    title: 'Settings',
    icon: FontIcons.dashboard,
    screen: Screens.Settings,
    children: []
  },
  {
    id:'WriteToUs',
    title: 'Write to us',
    icon: FontIcons.dashboard,
    screen: Screens.WriteToUs,
    children: []
  },
  {
    id:'TermsAndConditions',
    title: 'Terms & Conditions',
    icon: FontIcons.dashboard,
    screen: Screens.TermsAndConditions,
    children: []
  }
]




