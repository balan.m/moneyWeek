import React from 'react';
import _ from 'lodash';
import {StackNavigator} from 'react-navigation'
import {withRkTheme} from 'react-native-ui-kitten'
import {NavBar} from '../../components/index';
import transition from './transitions';
import {MoneyWeekRoutes} from './routes';

let main = {};
let flatRoutes = {};


(MoneyWeekRoutes).map(function (route, index) {

  let wrapToRoute = (route) => {
    return {
      screen: withRkTheme(route.screen),
      title: route.title
    }
  };

  flatRoutes[route.id] = wrapToRoute(route);
});

let ThemedNavigationBar = withRkTheme(NavBar);

const DrawerRoutes = Object.keys(flatRoutes).reduce((routes, name) => {
  let stack_name = name;
  console.log(stack_name);
  routes[stack_name] = {
    name: stack_name,
    screen: StackNavigator(flatRoutes, {
      initialRouteName: name,
      headerMode: 'screen',
      cardStyle: {backgroundColor: 'transparent'},
      transitionConfig: transition,
      navigationOptions: ({navigation, screenProps}) => ({
        gesturesEnabled: false,
        header: (headerProps) => {
          return <ThemedNavigationBar navigation={navigation} headerProps={headerProps}/>
        }
      })
    })
  };
  return routes;
}, {});

export const AppRoutes = DrawerRoutes;

