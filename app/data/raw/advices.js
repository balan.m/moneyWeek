 const advices =
  [{
    'id': 1,
    'name': 'BHEL',
    'typeoforder': 'BUY',
    'buyamount': 762,
    'target': 780,
    'date': '07/19/2017 10:12:00',
    'stopLoss': 759,
  }, {
    'id': 2,
    'name': 'Marut Fut',
    'typeoforder': 'BUY',
    'buyamount': 7476,
    'target': 7486,
    'date': '07/19/2017 09:12:00',
    'stopLoss': 7475,
  },  {
    'id': 3,
    'name': 'Bajaj Auto 2816',
    'typeoforder': 'BUY',
    'buyamount': 503,
    'target': 511,
    'date': '07/19/2017 09:15:00',
    'stopLoss': 502,
  },  {
    'id': 4,
    'name': 'FRETAIL',
    'typeoforder': 'SELL',
    'buyamount': 1200,
    'target': 1190,
    'date': '07/19/2017 09:22:00',
    'stopLoss': 1201,
  }];

export default advices;