const baseURL ='http://localHost:8080/advice/addAdvice';

export let addAdvice = (tradeName,tradeType,priceValue) => fetch('$(baseURL)?tradeName=${tradeName},tradeType=${tradeType},priceValue={priceValue}',{
	method: 'POST',
	headers: {
		'Accept': 'application/json',
		'contentType': 'application/json'
	}
})
.then((response) => response.json())